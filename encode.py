#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
import pathlib
import random


def main(**kwargs):
    data = None
    with pathlib.Path("./chiffre.json").open("r") as chiffre:
        data = json.load(chiffre)
    message = input("Nachricht eingeben: ")
    tmp = message.lower()
    for item in data.keys():
        tmp = tmp.replace(item, random.choice(data[item]))
        print("{} ({})".format(tmp, message))


if __name__ == "__main__":
    main()