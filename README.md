# MageAcademy Runengenerator
Ein kleiner Generator um normalen Text in unicode Runenzeichen umzuwandeln.

## Installation:
Bitte installiere Python3. [Hier kann man das runterladen.](https://www.python.org/downloads/) 
- Clicke beim Installer auf "Install now". 
- Setze den Haken bei "Add python to PATH".

Normalerweise sollte es ausreichen, wenn du danach auf die Datei `encode.py` doppelclickst.